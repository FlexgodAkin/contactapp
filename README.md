# contactApp

Simple Node JS React JS Contact CRUD Application.

## Getting Started


Please ensure Docker and Docker Compose are installed.

You can get Docker [Here](https://www.docker.com/get-docker)

You can get Docker Compose [Here](https://docs.docker.com/compose/install/)

```

  1.  git clone https://FlexgodAkin@bitbucket.org/FlexgodAkin/contactapp.git
  2.  cd contactapp
  4.  docker-compose build
  5.  docker-compose up
  6.  visit http://localhost:3000 to view!

```

The above will get you a copy of the project up and running on your local machine for development and testing purposes.


## Dependencies

```
  1. Docker
  2. Docker Compose
```


## License

This project is licensed under the MIT License - see the [LICENSE.md](https://opensource.org/licenses/MIT) file for details
