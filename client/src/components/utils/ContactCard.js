import React from 'react';
import moment from 'moment';
import { Link } from 'react-router-dom';

const ContactCard = (props) => {
  const { details } = props;
  const editUrl = `/edit/${details.id}`;
  const deleteUrl = `/delete/${details.id}`;
  const created_at = moment(details.created_at).format('YYYY-MM-DD');
  const editTo = {
    pathname: editUrl,
    full_name: details.full_name,
    email: details.email,
    phone: details.phone,
    address: details.address
  }
  const deleteTo = {
    pathname: deleteUrl ,
    full_name: details.full_name,
    email: details.email,
    phone: details.phone,
    address: details.address
  }
  return (
    <tr>
      <td>{details.full_name}</td>
      <td>{details.phone}</td>
      <td>{details.email}</td>
      <td>{details.address}</td>
      <td>{created_at}</td>
      <td><Link to={editTo}>Edit</Link></td>
      <td><Link to={deleteTo}>Delete</Link></td>
    </tr>
  );
};


export default ContactCard;