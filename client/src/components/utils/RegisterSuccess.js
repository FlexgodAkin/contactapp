import React from 'react';
import { Link } from 'react-router-dom';
import Header from '../views/Header';

const RegisterSuccess = props => (
  <div>
    <Header />
    <div className="container text-center">
      <br />
      <br />
      <h1>
        <i className="fa fa-4x fa-check-circle text-success"></i>
      </h1>
      <h1>
          Registration Successful
      </h1>
      <br />
      <h5>
        Please Login to continue
      </h5>
      <br/>
      <Link to="/login" className="btn btn-yellow">
        LOGIN
      </Link>
    </div>
  </div>
);

export default RegisterSuccess;
