import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import Header from './views/Header';


import { resetState, editContact } from '../actions/index';


function mapStateToProps(state) {
  return {
    editContactSuccess: state.editContactSuccess,
    isError: state.isError,
    isLoading: state.IsLoading,
    errorMessage: state.errorMessage
  };
}


class EditContact extends Component {

  state = {
    noContact: false,
  }


  componentDidMount() {
    this.props.resetState();
    if(!this.props.location.full_name) {
      this.setState({
        noContact: true
      })
    }
  }


  handleSubmit = e => {
    e.preventDefault();
        const { contact_id } = this.props.match.params;
          let full_name = this.full_name.value;
          let phone = this.phone.value;
          let address = this.address.value;
          if(!full_name.trim()) {
            full_name = this.props.location.full_name
          };
          if(!phone.trim()) {
            phone = this.props.location.phone
          };
          if(!address.trim()) {
            address = this.props.location.address
          };
          const payload = {
            full_name,
            phone,
            address
          };
        this.props.editContact(contact_id, payload);
   };


  render() {
    const newUrl = `/main`
    let view = <div />;

    if(this.state.noContact) {
      view = (<Redirect to="/main"/>);
    }
    if( this.props.isError) {
      view = (
        <div className="container text-center">
          <div className="row">
            <div className="col-lg-12 text-danger">
              <br />
              {'Something went wrong'}
            </div>
          </div>
        </div>
      );
    } else if (this.props.isLoading) {
      view = (
        <div className="container text-center">
          <div className="row">
            <div className="col-lg-12">
              <br />
              <i className="fa fa-2x fa-circle-o-notch fa-spin" />
            </div>
          </div>
        </div>
      );
      } else if(this.props.editContactSuccess) {
        view = (<Redirect to={newUrl} />)
      }
    return (
      <div>
        <Header />
            <div className="container">
            <div className="row text-center">
              <div className="col-lg-12">
                <h4>
                  Update Contact
                </h4>
                <br />
                <br />
              </div>
          </div>
          <div className="row">
            <div className="col-lg-3 col-md-2 col-sm-2 col-xs-12" />
            <div className="col-lg-6 col-md-8 col-sm-8 col-xs-12">
              <form onSubmit={this.handleSubmit}>
                <div className="form-group">
                  <input
                    type="text"
                    name="full_name"
                    className="form-control"
                    onChange={this.handleInputChange}
                    ref={(ref) => { this.full_name= ref; }}
                    placeholder={this.props.location.full_name}
                  />
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    name="email"
                    className="form-control "
                    onChange={this.handleInputChange}
                    ref={(ref) => { this.email = ref; }}
                    placeholder={this.props.location.email}
                    readOnly
                  />
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    name="phone"
                    className="form-control"
                    onChange={this.handleInputChange}
                    ref={(ref) => { this.phone = ref; }}
                    placeholder={this.props.location.phone}
                  />
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    name="address"
                    className="form-control"
                    onChange={this.handleInputChange}
                    ref={(ref) => { this.address= ref; }}
                    placeholder={this.props.location.address}
                  />
                </div>
                <div className="form-group">
                  <button className="btn btn-block btn-success">
                  Update Contact
                  </button>
                </div>
              </form>
              {view}
            </div>
            <div className="col-lg-3 col-md-2 col-sm-2 col-xs-12" />
          </div>
        </div>
        </div>
    );
  }
}


export default connect(mapStateToProps, { resetState, editContact})(EditContact);
