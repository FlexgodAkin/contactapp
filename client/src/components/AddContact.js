import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import Header from './views/Header';
import { createContact, resetState } from '../actions/';


function mapStateToProps(state) {
  return {
    isError: state.isError,
    isLoading: state.loginIsLoading,
    errorMessage: state.errorMessage,
    createContactSuccess: state.createContactSuccess
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchData: (payload) => dispatch(createContact(payload)),
    resetData: () => dispatch(resetState()),
  };
}


class AddContact extends Component {
  constructor() {
    super();
    this.onSubmit = this.onSubmit.bind(this);
    this.state = {
        inputError: ''
      };
  }

  componentWillMount() {
    this.props.resetData();
  }


  onSubmit(event) {
    event.preventDefault();
    if(!this.full_name.value) {
        this.setState({
          inputError: 'Please provide a name for the contact'
        })
        return;
    };
    if(!this.email.value) {
        this.setState({
          inputError: 'Please provide a valid email address'
        })
        return;
      };
    if(!this.phone.value) {
        this.setState({
          inputError: 'Please provide a phone number for the contact'
        })
        return;
    };
    if(!this.address.value) {
      this.setState({
        inputError: 'Please provide an address for the contact'
      })
      return;
  };
    this.setState({
        inputError: ''
    });
    const payload = {
      full_name: this.full_name.value,
      email: this.email.value,
      phone: this.phone.value,
      address: this.address.value,
    };
    this.props.fetchData(payload);
  }


  render() {
    const { errorMessage, isError, isLoading, createContactSuccess  } = this.props;
    let view = '';
    let inputError = (<p></p>);
    if(this.state.inputError !== '') {
        inputError = (<p className="text-danger">{this.state.inputError}</p>)
      }
    if (isError ) {
      view = errorMessage;
    } else if (isLoading) {
      view = <i className="fa fa-2x fa-circle-o-notch fa-spin text-danger" />;
    } else if(createContactSuccess) {
      view = <Redirect to="/main" />;
    }
    return (
      <div>
        <Header />
        <div className="container text-center">
          <div className="row">
            <div className="col-lg-12">
              <h6>
                Create New Contact
              </h6>
              <br />
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3 col-md-2 col-sm-2 col-xs-12" />
            <div className="col-lg-6 col-md-8 col-sm-8 col-xs-12">
              <form onSubmit={this.onSubmit}>
                <div className="form-group">
                  <input
                    ref={(input) => { this.full_name = input; }}
                    type="text"
                    className="form-control create-news-form-input"
                    placeholder="Full Name"
                  />
                </div>
                <div className="form-group">
                  <input
                    ref={(input) => { this.email = input; }}
                    type="text"
                    className="form-control create-news-form-input"
                    placeholder="Email"
                  />
                </div>
                <div className="form-group">
                  <input
                    ref={(input) => { this.phone = input; }}
                    type="text"
                    className="form-control create-news-form-input"
                    placeholder="Phone Number"
                  />
                </div>
                <div className="form-group">
                  <input
                    ref={(input) => { this.address = input; }}
                    type="text"
                    className="form-control create-news-form-input"
                    placeholder="Address"
                  />
                </div>
                <div className="form-group">
                  <button
                    className="btn btn-primary btn-block "
                  >
                  Create Contact
                  </button>
                </div>
                <div className="form-group text-danger">
                 {inputError}
                </div>
                <div className="form-group text-danger">
                  {view}
                </div>
              </form>
            </div>
            <div className="col-lg-3 col-md-2 col-sm-2 col-xs-12" />
          </div>
        </div>
        </div>
    );
  }
}

  export default connect(mapStateToProps, mapDispatchToProps)(AddContact);
