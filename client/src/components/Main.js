import React, { Component } from 'react';
import { connect } from 'react-redux';
import Header from './views/Header';
import ContactCard from './utils/ContactCard';
import { fetchContacts } from '../actions/';


function mapStateToProps(state) {
  return {
    isError: state.isError,
    isLoading: state.isLoading,
    errorMessage: state.errorMessage,
    contacts: state.fetchContactsSuccess
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchData: () => dispatch(fetchContacts()),
  };
}


class Main extends Component {
  componentDidMount() {
    this.props.fetchData();
  }
  render() {
    let view = <div />;
    if(this.props.isError) {
      view = (
            <p>
              There Was an Error Loading Contacts Data
            </p>
      );
    } else if (this.props.isLoading) {
      view = (
              <i className="fa fa-2x fa-circle-o-notch fa-spin text-danger" />
      );
    } else {
      const { contacts } = this.props;
      if(contacts.length < 1) {
        view = (
              <p>
                No Contact added yet.
              </p>
            )
      } else {
        view = (
          <div>
          <h4 className="text-center">Contact List</h4>
          <br />
          <table className="table table-dark table-striped">
            <thead>
              <tr>
                <th>FullName</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Address</th>
                <th>Added At</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {Object.keys(contacts).map(contact => <ContactCard key={contact} details={contacts[contact]} />)}
          </tbody>
        </table>
        </div>
        );
      }
    }
    return (
      <div>
        <Header />
            <div className="container">
            <div className="row">
                <div className="col-lg-12">
                    {view}
                </div>
            </div>
        </div>
        </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);
