import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import Header from './views/Header';


import { resetState, deleteContact } from '../actions/index';


function mapStateToProps(state) {
  return {
    deleteContactSuccess: state.deleteContactSuccess,
    isError: state.isError,
    isLoading: state.IsLoading,
    errorMessage: state.errorMessage
  };
}



class DeleteContact extends Component {
  state = {
    noContact: false,
  }

  componentDidMount() {
    this.props.resetState();
    if(!this.props.location.full_name) {
      this.setState({
        noContact: true
      })
    }
  }

  handleSubmit = e => {
    e.preventDefault();
        const { contact_id } = this.props.match.params;
        this.props.deleteContact(contact_id);
   };


  render() {
    const newUrl = '/main';
    let view = <div />;

    if(this.state.noContact) {
      view = (<Redirect to="/main"/>);
    }
    if(this.props.isError) {
      view = (
        <div className="container text-center">
          <div className="row">
            <div className="col-lg-12 text-danger">
              <br />
              {'Something went wrong'}
            </div>
          </div>
        </div>
      );
    } else if (this.props.isLoading) {
      view = (
        <div className="container text-center">
          <div className="row">
            <div className="col-lg-12">
              <br />
              <i className="fa fa-2x fa-circle-o-notch fa-spin" />
            </div>
          </div>
        </div>
      );
      } else if(this.props.deleteContactSuccess) {
        view = (<Redirect to={newUrl} />)
      }
    return (
      <div>
        <Header />
            <div className="container">
            <div className="row text-center">
            <div className="col-lg-12">
              <h4>
                Are you sure you want to delete this Contact?
              </h4>
              <br />
              <br />
            </div>
            </div>
            <div className="row">
            <div className="col-lg-3 col-md-2 col-sm-2 col-xs-12" />
            <div className="col-lg-6 col-md-8 col-sm-8 col-xs-12">
              <form onSubmit={this.handleSubmit}>
                <div className="form-group">
                  <input
                    type="text"
                    name="full_name"
                    className="form-control"
                    onChange={this.handleInputChange}
                    ref={(ref) => { this.full_name = ref; }}
                    placeholder={this.props.location.full_name}
                    readOnly
                  />
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    name="email"
                    className="form-control "
                    onChange={this.handleInputChange}
                    ref={(ref) => { this.email = ref; }}
                    placeholder={this.props.location.email}
                    readOnly
                  />
                </div>
                <div className="form-group">
                  <button className="btn btn-block btn-danger">
                    Delete Contact
                  </button>
                </div>
              </form>
              {view}
            </div>
            <div className="col-lg-3 col-md-2 col-sm-2 col-xs-12" />
          </div>
        </div>
        </div>
    );
  }
}

export default connect(mapStateToProps, { resetState, deleteContact })(DeleteContact);
