import {
    IS_LOADING,
    IS_ERROR,
    ERROR_MESSAGE
  } from '../actions/types';


  export function isError(state = false, action) {
    switch (action.type) {
      case IS_ERROR:
        return action.isError;
      default:
        return state;
    }
  }


  export function errorMessage(state = null, action) {
    switch (action.type) {
      case ERROR_MESSAGE:
        return action.errorMessage;
      default:
        return state;
    }
  }


  export function isLoading(state = false, action) {
    switch (action.type) {
      case IS_LOADING:
        return action.isLoading;
      default:
        return state;
    }
  }
