import {
  FETCH_CONTACTS_SUCCESS,
  CREATE_CONTACT_SUCCESS,
  DELETE_CONTACT_SUCCESS,
  EDIT_CONTACT_SUCCESS
} from '../actions/types';

export function fetchContactsSuccess(state = [], action) {
  switch (action.type) {
    case FETCH_CONTACTS_SUCCESS:
      return action.contacts;
    default:
      return state;
  }
}


export function createContactSuccess(state = false, action) {
  switch (action.type) {
    case CREATE_CONTACT_SUCCESS:
      return action.createContactSuccess;
    default:
      return state;
  }
}



export function deleteContactSuccess(state = false, action) {
  switch (action.type) {
    case DELETE_CONTACT_SUCCESS:
      return action.deleteContactSuccess;
    default:
      return state;
  }
}


export function editContactSuccess(state = false, action) {
  switch (action.type) {
    case EDIT_CONTACT_SUCCESS:
      return action.editContactSuccess;
    default:
      return state;
  }
}
