import {
    LOGIN_SUCCESS,
    AUTHENTICATED,
    REGISTER_SUCCESS
  } from '../actions/types';

  export function authenticated(state = false, action) {
    switch (action.type) {
      case AUTHENTICATED:
        return action.authenticated;
      default:
        return state;
    }
  }



  export function loginSuccess(state = {}, action) {
    switch (action.type) {
      case LOGIN_SUCCESS:
        return action.user;
      default:
        return state;
    }
  }


  export function registerSuccess(state = false, action) {
    switch (action.type) {
      case REGISTER_SUCCESS:
        return action.registerSuccess;
      default:
        return state;
    }
  }
