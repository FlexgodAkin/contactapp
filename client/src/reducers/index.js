import { combineReducers } from 'redux';

import { isError, isLoading, errorMessage} from './utils';
import { loginSuccess, authenticated, registerSuccess } from './auth';
import { createContactSuccess, fetchContactsSuccess, deleteContactSuccess, editContactSuccess } from './contact';

export default combineReducers({
  isError,
  isLoading,
  errorMessage,
  loginSuccess,
  authenticated,
  registerSuccess,
  createContactSuccess,
  fetchContactsSuccess,
  deleteContactSuccess,
  editContactSuccess
});
