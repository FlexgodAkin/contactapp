import axios from 'axios';
import config from '../config';
import {
  IS_LOADING,
  IS_ERROR,
  ERROR_MESSAGE,
  AUTHENTICATED,
  LOGIN_SUCCESS,
  REGISTER_SUCCESS,
  FETCH_CONTACTS_SUCCESS,
  CREATE_CONTACT_SUCCESS,
  DELETE_CONTACT_SUCCESS,
  EDIT_CONTACT_SUCCESS
} from './types';

const baseApiUrl = `${config.baseApiUrl}`;


export function authenticated(bool) {
    return {
      type: AUTHENTICATED,
      authenticated: bool,
  };
};


export const isLoading = (bool) => {
  return {
    type: IS_LOADING,
    isLoading: bool
  }
};


export const isError = (bool) => {
  return {
    type: IS_ERROR,
    isError: bool
  }
};


export const errorMessage = (errorMessage) => {
  return {
    type: ERROR_MESSAGE,
    errorMessage
  }
};


export const loginSuccess = (user) => {
    return {
        type: LOGIN_SUCCESS,
        user
    }
};


export const registerSuccess = (bool) => {
    return {
        type: REGISTER_SUCCESS,
        registerSuccess: bool
    }
};



export const fetchContactsSuccess = (contacts) => {
  return {
      type: FETCH_CONTACTS_SUCCESS,
      contacts
  }
};


export const createContactSuccess = (bool) => {
  return {
      type: CREATE_CONTACT_SUCCESS,
      createContactSuccess: bool
  }
};


export const deleteContactSuccess = (bool) => {
  return {
    type: DELETE_CONTACT_SUCCESS,
    deleteContactSuccess: bool
  }
}


export const editContactSuccess = (bool) => {
  return {
    type: EDIT_CONTACT_SUCCESS,
    editContactSuccess: bool
  }
}


export  const loginUser = (payload) => {
return async(dispatch) => {
    dispatch(isLoading(true));
    try {
    const response = await axios.post(`${baseApiUrl}/login`, payload);
    const { data } = response;
    const { token, email } = data;
    localStorage.setItem('token',token);
    dispatch(loginSuccess(email));
    dispatch(authenticated(true));
    } catch(e) {
        if(e.response && e.response.data){
        dispatch(isError(true));
        dispatch(errorMessage(e.response.data.message));
        } else {
            dispatch(isError(true));
            dispatch(errorMessage('Something went wrong'));
        }
    }
  }
};


export  const registerUser = (payload) => {
    return async(dispatch) => {
        dispatch(isLoading(true));
        try {
        await axios.post(`${baseApiUrl}/register`, payload);
        dispatch(registerSuccess(true));
        } catch(e) {
          console.log(e.response)
            if(e.response && e.response.data){
            dispatch(isError(true));
            dispatch(errorMessage(e.response.data.message));
            } else {
                dispatch(isError(true));
                dispatch(errorMessage('Something went wrong'));
            }
        }
    }
};



export  const fetchContacts = () => {
  return async(dispatch) => {
      dispatch(isLoading(true));
      try {
        const token = localStorage.getItem('token');
        const payload = {
          headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          authorization: token,
        },
      };
      const response = await axios.get(`${baseApiUrl}/contact`, payload);
      const { data: { contacts } } = response;
      dispatch(resetState());
      dispatch(fetchContactsSuccess(contacts));
      } catch(e) {
          if(e.response && e.response.data && e.response.data.message){
            console.log(e.response.data)
          dispatch(isError(true));
          dispatch(errorMessage(e.response.data.message));
          } else {
              dispatch(isError(true));
              dispatch(errorMessage('Something went wrong'));
          }
      }
  }
};



export  const createContact = (body) => {
  return async(dispatch) => {
      dispatch(isLoading(true));
      try {
        const token = localStorage.getItem('token');
        const payload = {
          headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          authorization: token,
        },
      };
      await axios.post(`${baseApiUrl}/contact`, body, payload);
      dispatch(createContactSuccess(true));
      } catch(e) {
          if(e.response && e.response.data){
          dispatch(isError(true));
          dispatch(errorMessage(e.response.data.message));
          } else {
              dispatch(isError(true));
              dispatch(errorMessage('Something went wrong'));
          }
      }
  }
};



export const deleteContact = (contact_id) => {
  return async (dispatch) => {
    dispatch(isError(false));
    dispatch(isLoading(true));
    try {
      const token = localStorage.getItem('token');
      const payload = {
        headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        authorization: token,
      },
    };
    await axios.delete(`${baseApiUrl}/contact/${contact_id}`, payload);
    dispatch(deleteContactSuccess(true));
    } catch(e) {
      if(e.response && e.response.data){
        dispatch(isError(true));
        dispatch(errorMessage(e.response.data));
        } else {
            dispatch(isError(true));
            dispatch(errorMessage('Something went wrong'));
        }
    }
  };
};



export const editContact = (contact_id, body) => {
  return async (dispatch) => {
    dispatch(isError(false));
    dispatch(isLoading(true));
    dispatch(editContactSuccess(false));
    try {
      const token = localStorage.getItem('token');
      const payload = {
        headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        authorization: token,
      },
      };
      await axios.put(`${baseApiUrl}/contact/${contact_id}`, body, payload);
      dispatch(editContactSuccess(true));
    } catch(e) {
      if(e.response && e.response.data){
        dispatch(isError(true));
        dispatch(errorMessage(e.response.data));
        } else {
            dispatch(isError(true));
            dispatch(errorMessage('Something went wrong'));
        }
    }
  };
};



export default function logoutUser() {
    localStorage.clear();
    return (dispatch) => {
      dispatch(isError(false));
      dispatch(errorMessage(null));
      dispatch(loginSuccess({}));
      dispatch(authenticated(false));
    };
  };


export function resetState() {
  return (dispatch) => {
    dispatch(isError(false));
    dispatch(isLoading(false));
    dispatch(errorMessage(null));
    dispatch(createContactSuccess(false));
    dispatch(editContactSuccess(false));
    dispatch(deleteContactSuccess(false));
  };
};


