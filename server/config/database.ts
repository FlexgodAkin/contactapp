import promise from 'bluebird';
import {IMain, IDatabase} from 'pg-promise';
import * as pgPromise from 'pg-promise';

const pgp:IMain = pgPromise({
  promiseLib: promise,
  noLocking: true
});

const cn:string = process.env.CONTACT_DATABASE_URL;

const db:IDatabase<any> = pgp(cn);

export default db;