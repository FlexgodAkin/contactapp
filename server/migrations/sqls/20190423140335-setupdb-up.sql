/* Replace with your SQL commands */
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE user_details(
    id VARCHAR PRIMARY KEY DEFAULT 'user-' || LOWER(
            REPLACE(
                CAST(uuid_generate_v1mc() As varchar(50))
                , '-','')
            ),
    email VARCHAR(50) UNIQUE NOT NULL,
    hash VARCHAR(200),
    salt VARCHAR(200),
    created_at TIMESTAMPTZ NOT NULL,
    updated_at TIMESTAMPTZ
);


CREATE TABLE contact(
    id VARCHAR PRIMARY KEY DEFAULT 'contact-' || LOWER(
            REPLACE(
                CAST(uuid_generate_v1mc() As varchar(50))
                , '-','')
            ),
    user_id VARCHAR(50) NOT NULL REFERENCES user_details(id) ON UPDATE CASCADE,
    full_name VARCHAR(50)  NOT NULL,
    email VARCHAR(50) NOT NULL,
    created_at TIMESTAMPTZ NOT NULL,
    updated_at TIMESTAMPTZ
);



CREATE TABLE contact_details(
    id VARCHAR PRIMARY KEY DEFAULT 'cont-deta-' || LOWER(
            REPLACE(
                CAST(uuid_generate_v1mc() As varchar(50))
                , '-','')
            ),
    contact_id VARCHAR(50) NOT NULL REFERENCES contact(id) ON UPDATE CASCADE,
    phone VARCHAR(50) NOT NULL,
    address VARCHAR(2000) NOT NULL,
    created_at TIMESTAMPTZ NOT NULL,
    updated_at TIMESTAMPTZ
);


INSERT INTO user_details(email, hash, salt, created_at, updated_at)
VALUES('akin@gmail.com', '$2b$10$tg3VgeHS81F1A00v9gB7xOCVMf/kZ0ybSOMAfjQUTeXR1hIjF9sTy', '$2b$10$tg3VgeHS81F1A00v9gB7xO', NOW(), NOW());
