const queries = {
    getAllContacts: `
        SELECT
            contact.id,
            contact.email,
            contact.full_name,
            contact_details.phone,
            contact_details.address,
            contact.created_at,
            contact.updated_at
        FROM
            contact
        LEFT JOIN contact_details ON contact_details.contact_id = contact.id
        WHERE
            contact.user_id = $1
    `,
    getContactById: `
        SELECT
            contact.id,
            contact.email,
            contact.full_name,
            contact_details.phone,
            contact_details.address,
            contact.created_at,
            contact.updated_at
        FROM
            contact
        LEFT JOIN contact_details ON contact_details.contact_id = contact.id
        WHERE
          contact.id = $1
        AND
          contact.user_id = $2
    `,
    getContactByEmail: `
        SELECT
          *
        FROM
          contact
        WHERE
          email = $1
        AND
          user_id = $2
    `,
    updateContact: `
          UPDATE
              contact
          SET
              full_name = $1
          WHERE
              id = $2
    `,
    updateContactDetails: `
          UPDATE
              contact_details
          SET
              phone = $1,
              address = $2
          WHERE
              contact_id = $3
    `,
    deleteContact: `
        DELETE
        FROM
            contact
        WHERE id = $1
    `,
    deleteContactDetails: `
        DELETE
        FROM
            contact_details
        WHERE contact_id = $1
    `,
    createUser: `
        INSERT INTO user_details(email, hash, salt, created_at, updated_at) VALUES
        ($1, $2, $3, $4, $5)
    `,
    getUserByEmail: `
        SELECT
          *
        FROM
          user_details
        WHERE
          email = $1
    `,
    createContact: `
        INSERT INTO contact(user_id, full_name, email, created_at, updated_at) VALUES
        ($1, $2, $3, $4, $5) RETURNING id
    `,
    createContactDetails: `
        INSERT INTO contact_details(contact_id, phone, address, created_at, updated_at) VALUES
        ($1, $2, $3, $4, $5)
    `,
};

export default queries;
