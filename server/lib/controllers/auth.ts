import { Request, Response, NextFunction } from 'express';
import * as moment from 'moment';
import * as  jwt from 'jsonwebtoken';
import * as bcrypt from 'bcrypt';
import * as Joi from 'joi';
import db from '../../config/database';
import query from '../../queries';
import config from '../../config';

export class AuthController {

public async loginUser (req: Request, res: Response) {
      const { body: {email, password } } = req;
      try {
        const userSchema = Joi.object({
          email: Joi.string().email().required(),
          password: Joi.string().required()
        });
        await userSchema.validate(req.body);
        const user = await db.oneOrNone(query.getUserByEmail, [email]);
        if(!user) {
          res.status(400).json({message: 'Invalid Email/Password'});
          return;
        }
        const { hash, salt } = user;
        bcrypt.hash(password, salt, (err, passwordHash) => {
          if (err) {
            res.status(500).json({message: 'Unknown Error. Please try again'});
            return;
          }
          if(passwordHash !== hash) {
            res.status(400).json({message: 'Invalid Email/Password'});
            return;
          }
          const userDetails = {
            id: user.id,
            email: user.email
          };
          const token = jwt.sign(userDetails, config.jwtSecret,
            {
              expiresIn: '24h' // expires in 24 hours
            });
          res.status(200).json({
              success: true,
              message: 'Authentication successful!',
              email: user.email,
              token
            })
        });
      } catch(e) {
        res.status(400).json({message: 'Error creating user', e});
      }
    }


public async registerUser (req: Request, res: Response) {
      const { body: {email, password, password_confirm } } = req;
      const saltRounds = 10;
      try {
        const userSchema = Joi.object({
          email: Joi.string().email().required(),
          password: Joi.string().required(),
          password_confirm: Joi.string().required()
        });
        await userSchema.validate(req.body);
        const user = await db.oneOrNone(query.getUserByEmail, [email]);
        if(user) {
          res.status(400).json({message: 'A user with that email exists already'})
          return;
        }
        if(password !== password_confirm) {
          res.status(400).json({message: 'Passwords do not match'});
          return;
        }
        bcrypt.genSalt(saltRounds, (e, salt) => {
          bcrypt.hash(password, salt, async (err, hash) => {
            if (err) {
              res.status(500).json({message: 'Unknown Error. Please try again'});
              return;
            }
        await db.none(query.createUser, [email, hash, salt, moment(), moment()]);
          res.json({
              success: true,
              message: 'User Created'
            });
          });
        });
      } catch(e) {
        res.status(400).json({message: 'Error creating user', e});
      }
  }


public async verifyToken (req: Request, res: Response, next: NextFunction) {
    const token = req.headers['x-access-token'] || req.headers['authorization'];
    if (token) {
      jwt.verify(token, config.jwtSecret, (err, decoded) => {
        if (err) {
          return res.json({
            success: false,
            message: 'Token is not valid'
          });
        } else {
          res.locals.decoded = decoded;
          next();
        }
      });
    } else {
        return res.status(401).json({
          success: false,
          message: 'Auth token is not supplied'
      });
    }
  }
}