import { Request, Response } from 'express';
import * as moment from 'moment';
import * as Joi from 'joi';
import db from '../../config/database';
import query from '../../queries';


export class ContactController {


public async addNewContact (req: Request, res: Response) {
      const user_id = res.locals.decoded.id;
      const { body: { full_name, email, phone, address } } = req;
       try {
        const userSchema = Joi.object({
          full_name: Joi.string().required(),
          email: Joi.string().email().required(),
          phone: Joi.string().required(),
          address: Joi.string().required()
        });
        await userSchema.validate(req.body);
          const result = await db.oneOrNone(query.getContactByEmail, [email, user_id]);
          if(result) {
            res.status(400).json({message: 'A contact with that email exists already'})
            return;
          }
          const contact = await db.oneOrNone(query.createContact, [user_id, full_name, email, moment(), moment()]);
          const contact_id = contact.id;
          await db.none(query.createContactDetails, [contact_id, phone, address, moment(), moment()]);
          res.status(201).json({
            success: true,
            message: 'Contact Created',
            contact
          })
       } catch(e) {
        res.status(400).json({message: 'Error creating contact', e});
       }
    }


public async getAllContacts (req: Request, res: Response) {
      const user_id = res.locals.decoded.id;
      try {
        const result = await db.any(query.getAllContacts, [user_id]);
        res.status(200).json({contacts: result});
      } catch(e) {
        res.status(400).json(e)
      };
  }


public async getContactById (req: Request, res: Response) {
    const user_id = res.locals.decoded.id;
    const { params: { contact_id } } = req;
    try {
      const contact = await db.oneOrNone(query.getContactById, [contact_id, user_id]);
      if(!contact) {
        res.status(400).json({
          message: 'No Contact with that id'
        });
        return;
      }
      res.status(200).json({ contact });
    } catch(e) {
      res.status(400).json(e)
    };
  }


public async updateContact (req: Request, res: Response) {
    const user_id = res.locals.decoded.id;
    const { params: { contact_id }, body: { full_name, phone, address } } = req;
    try {
      const userSchema = Joi.object({
        full_name: Joi.string().required(),
        phone: Joi.string().required(),
        address: Joi.string().required()
      });
      await userSchema.validate(req.body);
      const contact = await db.oneOrNone(query.getContactById, [contact_id, user_id]);
      if(!contact) {
        res.status(400).json({
          message: 'No Contact with that id'
        });
        return;
      }
        await db.none(query.updateContact, [full_name, contact_id]);
        await db.none(query.updateContactDetails, [ phone, address, contact_id]);
        res.status(200).json({
          message: 'Updated Contact Successfully'
        })
    } catch(e) {
      res.status(400).json({message: 'Error updating contact', e});
    }
  }


public async deleteContact (req: Request, res: Response) {
    const user_id = res.locals.decoded.id;
    const { params: { contact_id } } = req;
    try {
      const contact = await db.oneOrNone(query.getContactById, [contact_id, user_id]);
      if(!contact) {
        res.status(400).json({
          message: 'No Contact with that id'
        });
        return;
      }
      await db.none(query.deleteContactDetails, [contact_id]);
      await db.none(query.deleteContact, [contact_id]);
      res.status(200).json({ message: 'Contact Deleted' });
    } catch(e) {
      res.status(400).json(e)
    };
  }
}