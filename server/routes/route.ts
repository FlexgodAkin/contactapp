import {Request, Response} from "express";
import { ContactController } from "../lib/controllers/contact";
import { AuthController } from "../lib/controllers/auth";


export class Routes {
    public contactController: ContactController = new ContactController();
    public authController: AuthController = new AuthController();

    public routes(app): void {

        app.route('/')
        .get((req: Request, res: Response) => {
            res.status(200).send({
                message: 'GET request successful!!!!'
            })
        })


        app.route('/contact')
        .get(this.authController.verifyToken, this.contactController.getAllContacts)
        .post(this.authController.verifyToken,this.contactController.addNewContact);


        app.route('/contact/:contact_id')
        .get(this.authController.verifyToken, this.contactController.getContactById)
        .put(this.authController.verifyToken, this.contactController.updateContact)
        .delete(this.authController.verifyToken, this.contactController.deleteContact);


        app.route('/login')
        .post(this.authController.loginUser)


        app.route('/register')
        .post(this.authController.registerUser)
    }
}